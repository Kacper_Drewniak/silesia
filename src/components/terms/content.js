import React from "react"

export const Content = () => {
  return (
    <div className="content--wrapper">
      <a
        className="twoButtonContainer__button red mt-4"
        href="/documents/regulamin.pdf"
        target="_blank"
        download="regulamin"
      >
        Pobierz
      </a>
    </div>
  )
}
