import React from "react"

export const AvatarText = ({ text, src }) => {
  return (
    <div className="avatarText--container">
      <img className="avatarText--container__image" src={src} />
      <div className="avatarText--container__description">{text}</div>
    </div>
  )
}
