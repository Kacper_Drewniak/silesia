import React from "react"
import { Container } from "reactstrap"
import { AvatarText } from "./avatarText"
import { Link } from "gatsby"

export const OurTeam = () => {
  const logoText = (
    <>
      <div className="AVT--header">Witaj w Edu3Dkacja Silesia!</div>
      <br />
      <br />
      <div className="AVT--text">
        W Edu3Dkacja prowadzimy zajęcia dla dzieci, które rozwijają wyobraźnie
        przestrzenną i pozwalają na poznanie najnowocześniejszych technologii.
        Czy wiesz, że technologia druku 3D jest uważana za jedną z najbardziej
        przyszłościowych dziedzin rozwoju przemysłu na świecie? Już dziś druk 3D
        jest wykorzystywany w wielu branżach: w medycynie, w archeologii,
        motoryzacji, lotnictwie, budownictwie – a to dopiero POCZĄTEK ery druku
        3D!
      </div>
    </>
  )

  const adamText = (
    <>
      <div className="AVT--header">
        Adam Fober
        <div className="AVT--text">Trener</div>
      </div>
      <div className="AVT--text">
        <span className="AVT--bold">Motto:</span> "Cierpliwość, wytrwałość i pot
        tworzą niepokonaną kombinację sukcesu" <br />
        <span className="AVT--bold"> Pasja:</span> Nowoczesne technologie <br />
        <span className="AVT--bold"> Hobby:</span> Sport i muzyka.
        Wykształcenie: Technik Informatyk, Zarządzanie i Inżynieria Produkcji
        <br />
        <span className="AVT--bold"> Kilka słów o Adamie:</span> Jestem osobą
        spokojną i opanowaną, która krok po kroku dąży do swojego celu. Wierzę,
        że ciężka praca przyniesie w przyszłości owocne efekty oraz staram
        pomagać się innym, jeśli tylko mogę, bo wiem, że dobro wraca.
      </div>
    </>
  )

  const MlynText = (
    <>
      <div className="AVT--header">
        Adam Młynarczyk
        <div className="AVT--text">Trener</div>
      </div>
      <div className="AVT--text">
        <span className="AVT--bold">Motto:</span> „Nie rób drugiemu, co tobie
        niemiłe” <br />
        <span className="AVT--bold"> Pasja:</span> Dzielenie się wiedzą i
        doświadczeniem <br />
        <span className="AVT--bold"> Hobby:</span> Rękodzieło, malowanie
        miniaturowych figurek, gry komputerowe, podróżowanie
        <br />
        <span className="AVT--bold"> Wykształcenie:</span> Finanse i
        rachunkowość
        <br />
        <span className="AVT--bold"> Kilka słów o Adamie:</span> Kilka słów o
        Adamie: Jestem osobą pracowitą i zorganizowaną. Traktuję ludzi tak, jak
        sam chciałbym być traktowany. Lubię pomagać innym, gdy tego potrzebują.
        Chętnie prowadzę dyskusje, a spokój i cierpliwość pozwalają mi stale się
        rozwijać.
      </div>
    </>
  )

  const MariText = (
    <>
      <div className="AVT--header">
        Maria Majewska
        <div className="AVT--text">Trener</div>
      </div>
      <div className="AVT--text">
        <span className="AVT--bold">Motto:</span> „Żyj i zachowuj się tak, aby
        nie żałować, jeżeli jutrzejszy dzień okaże się twoim ostatnim”
        <br />
        <span className="AVT--bold"> Pasja:</span> Animacja najmłodszych <br />
        <span className="AVT--bold"> Hobby:</span> Aktorstwo, ogród,
        beletrystyka, żeglarstwo
        <br />
        <span className="AVT--bold"> Wykształcenie:</span> Filozofia
        <br />
        <span className="AVT--bold"> Kilka słów o Marii:</span>Jestem osobą
        ambitną i pełną pasji. Nigdy łatwo się nie poddaję. Każdy upadek jest
        doświadczeniem, z którego wyciągam wnioski na lepszą pszyszłość. Uśmiech
        i życzliwość to cechy, które pozwalają mi patrzec na życie z entuzjazem
        i energią do działania.
      </div>
    </>
  )

  const JuliaText = (
    <>
      <>
        <div className="AVT--header">
          Julia Młynarczyk
          <div className="AVT--text">Trenerka</div>
        </div>
        <div className="AVT--text">
          <span className="AVT--bold">Motto:</span> ,,Codziennie patrz na świat
          tak, jakbyś widział go po raz pierwszy”
          <br />
          <span className="AVT--bold"> Pasja:</span> Odkrywanie swoich
          możliwości i realizacja nowych wyzwań <br />
          <span className="AVT--bold"> Hobby:</span> Podróżowanie, aktywność
          fizyczna, koncerty i festiwale, oglądanie zagranicznych seriali
          <br />
          <span className="AVT--bold"> Wykształcenie:</span> Zarządzanie
          <br />
          <span className="AVT--bold"> Kilka słów o sobie:</span>Myślę, że
          jestem osobą dobrze zorganizowaną, lubię planować i doprowadzać te
          plany do skutku. Wszystko co robię, staram się wykonać najlepiej jak
          potrafię. Moim największym marzeniem jest podróż do Stanów
          Zjednoczonych
        </div>
      </>
    </>
  )

  const KlaudiaText = (
    <>
      <>
        <div className="AVT--header">
          Klaudia Wójtowicz
          <div className="AVT--text">Trenerka</div>
        </div>
        <div className="AVT--text">
          <span className="AVT--bold">Motto:</span> ,,Codziennie patrz na świat
          tak, jakbyś widział go po raz pierwszy”
          <br />
          <span className="AVT--bold"> Pasja:</span> Odkrywanie swoich
          możliwości i realizacja nowych wyzwań <br />
          <span className="AVT--bold"> Hobby:</span> Rękodzieło, malowanie
          miniaturowych figurek, gry komputerowe, podróżowanie
          <br />
          <span className="AVT--bold"> Wykształcenie:</span> Filozofia
          <br />
          <span className="AVT--bold"> Kilka słów o Marii:</span>Jestem osobą
          ambitną i pełną pasji. Nigdy łatwo się nie poddaję. Każdy upadek jest
          doświadczeniem, z którego wyciągam wnioski na lepszą pszyszłość.
          Uśmiech i życzliwość to cechy, które pozwalają mi patrzec na życie z
          entuzjazem i energią do działania.
        </div>
      </>
    </>
  )

  const danielText = (
    <>
      <>
        <div className="AVT--header">
          Daniel Kauder
          <div className="AVT--text">Trener</div>
        </div>
        <div className="AVT--text">
          <span className="AVT--bold">Motto:</span> „Rób to co kochasz a nie
          przepracujesz ani jednego dnia”
          <br />
          <span className="AVT--bold"> Pasja:</span> innowacyjne podejście do
          nauki najmłodszych <br />
          <span className="AVT--bold"> Hobby:</span> nowe technologie,
          wspinaczka
          <br />
          <span className="AVT--bold"> Wykształcenie:</span> Turystyka
          <br />
          <span className="AVT--bold"> Kilka słów o osobie:</span>Jestem osobą
          energiczną, komunikatywną lubiącą kontakt z ludźmi, ale cechuje mnie
          również duża cierpliwość i sukcesywne dążenie do celu. Od lat łączę
          swoje pasje ze szkoleniem najmłodszych, wcześniej jako trener
          wspinaczki teraz z zakresu druku 3D. Pasjonują mnie wszelkiego rodzaju
          nowinki techniczne, chętnie poznaję nowe technologie, a z jeszcze
          większą przyjemnością opowiadam o nich najmłodszym.
        </div>
      </>
    </>
  )

  const pawelText = (
    <>
      <>
        <div className="AVT--header">
          Paweł Zając
          <div className="AVT--text">Trenerka</div>
        </div>
        <div className="AVT--text">
          <span className="AVT--bold">Motto:</span> ,,Codziennie patrz na świat
          tak, jakbyś widział go po raz pierwszy”
          <br />
          <span className="AVT--bold"> Pasja:</span> Odkrywanie swoich
          możliwości i realizacja nowych wyzwań <br />
          <span className="AVT--bold"> Hobby:</span> Rękodzieło, malowanie
          miniaturowych figurek, gry komputerowe, podróżowanie
          <br />
          <span className="AVT--bold"> Wykształcenie:</span> Filozofia
          <br />
          <span className="AVT--bold"> Kilka słów o Marii:</span>Jestem osobą
          ambitną i pełną pasji. Nigdy łatwo się nie poddaję. Każdy upadek jest
          doświadczeniem, z którego wyciągam wnioski na lepszą pszyszłość.
          Uśmiech i życzliwość to cechy, które pozwalają mi patrzec na życie z
          entuzjazem i energią do działania.
        </div>
      </>
    </>
  )

  return (
    <Container>
      <AvatarText src="/images/team/logoAvatar.svg" text={logoText} />
      <h1 className="ourTeam--title">Nasza Drużyna:</h1>
      <AvatarText src="/images/team/MariaM.jpg" text={MariText} />
      <AvatarText src="/images/team/AdamF.jpg" text={adamText} />
      <AvatarText src="/images/team/AdamM.jpg" text={MlynText} />

      <AvatarText src="/images/team/JuliaM.jpg" text={JuliaText} />
      <AvatarText src="/images/team/DanielK.jpg" text={danielText} />

      <img src="/images/team/ourTeam.png" width="100%" />
      <div className="twoButtonContainer">
        <Link className="twoButtonContainer__button red" to="/oferta">
          Oferta
        </Link>
        <Link className="twoButtonContainer__button blue" to="/kontakt">
          Kontakt
        </Link>
      </div>
    </Container>
  )
}
