import React from "react"

export const FourButtons = () => {
  const buttons = [
    {
      label: "533 572 925",
      href: "tel:533-572-925",
      src: "phone",
    },
    {
      label: "biuro@silesia3d.pl",
      href: "tel:533-572-925",
      src: "mail",
    },
    {
      label: "EDU3DKACJA SILESIA",
      href: "https://www.facebook.com/Edu3Dkacja-Silesia-105139601850092",
      src: "fb",
    },
    {
      label: "@EDU3DKACJA.SILESIA",
      href: "https://instagram.com/edu3dkacja.silesia?utm_medium=copy_link",
      src: "insta",
    },
  ]

  return (
    <div className="fourButtons--container">
      {buttons.map(({ label, href, src }) => (
        <a href={href} className="fourButtons--container__button">
          <img src={`/images/${src}.svg`} />
          {label}
        </a>
      ))}
    </div>
  )
}
