import React from "react"
export const HeroContact = () => {
  return (
    <div className="heroContact--containers">
      <div className="heroContact--containers__text">
        Jeśli masz pytania, chcesz poznać nas bliżej, albo poznać jeszcze więcej
        szczegółów dotyczących naszych zajęć wybierz dowolną formę kontaktu
      </div>
      <img src="/images/child1.png" />
    </div>
  )
}
