import React from "react"
import { Container } from "reactstrap"

export const HeroLaboratory = () => {
  return (
    <Container>
      <div className="heroLaboratory--container">
        <div className="heroLaboratory--container__title">
          Drukarka 3D do szkoły oraz pozostałe
          <br />
          wyposażenie - wszystko zgodne
          <br />z wymogami programu!
        </div>
        <div className="heroLaboratory--container__background"></div>
        <img
          className="heroLaboratory--container__icon"
          src="/images/child1.png"
        />
      </div>
    </Container>
  )
}
