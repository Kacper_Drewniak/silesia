import React from "react"
import { Col, Row } from "reactstrap"

export const TogetherLaboratory = () => {
  const Item = ({ index, title, text }) => (
    <Col sm="3" className="together--container__item">
      <img src={`/images/together/${index}.png`} />
      <h1 className="together--container__title">{title}</h1>
      <p className="text-center">{text}</p>
    </Col>
  )
  const cols = [
    {
      title: "Gwarancja zakupu osprzętu",
      text: "zgodnego z katalogiem wyposażenia udostepnionym przez rząd - a tym samym - w calosci finanoswego przez środki zewnętrzne",
    },
    {
      title: "Dostęp 60 scenariuszy lekcji",
      text: "interdyscyplinarnych lekcji gotowych do wdrożenia i zgodnych z podstawą programową MEN",
    },
    {
      title: "Kompleksowe szkolenie",
      text: "dla nauczycieli z obsługi zakupionego sprzętu (30h zajęć)",
    },
  ]

  return (
    <>
      <h1 className="together--container__header">
        Współpraca z edu3Dkacja Silesia, to:
      </h1>
      <Row className="together--container">
        {cols.map(({ title, text }, index) => (
          <Item index={index + 1} text={text} title={title} />
        ))}
      </Row>
    </>
  )
}
