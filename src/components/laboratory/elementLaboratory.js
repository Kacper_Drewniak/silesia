import React from "react"
import { Container } from "reactstrap"

export const ElementLaboratory = () => {
  const lessons = [
    {
      title: "Drukarka 3D oraz inne elementy\n" + "wyposażenia podstawowego!",
      text: "Zapewniamy współpracę gwarantującą realizację wszystkich założeń programu Laboratoria Przyszłości. Skontaktuj się z nami już dziś i porozmawiaj z naszym konsultantem. Pomożemy Ci: ✅ spełnić wszystkie niezbędne kryteria programowe ✅ dotrzymamy terminów warunkujących wypłatę wsparcia ✅ zapewniamy szkolenie dla nauczycieli i gotowe interdyscyplinarne konspekty zajęć (pamiętaj, że wymogiem programu Laboratoria Przyszłości jest prowadzenie lekcji z wykorzystaniem druku 3D) Nasz towar posiada wszystkie niezbędne certyfikaty oraz odpowiednio długi okres gwarancyjny.",
    },
  ]
  const Item = ({ title, text, imgSrc, button, revert }) => (
    <div className={`lessonsContainer__itemContainer`}>
      <img src={imgSrc} className="lessonsContainer__itemContainer--image" />
      <div className={`lessonsContainer__itemContainer--content`}>
        <h1 className="lessonsContainer__itemContainer--title">{title}</h1>
        <p className="lessonsContainer__itemContainer--description">{text}</p>
        {button}
      </div>
    </div>
  )

  return (
    <Container className="lessonsContainer">
      {lessons.map(({ imgSrc, title, text, button }, index) => (
        <Item
          imgSrc={`/images/lessons/item${index}.png`}
          title={title}
          text={text}
          revert={index % 2}
          button={button}
        />
      ))}
    </Container>
  )
}
