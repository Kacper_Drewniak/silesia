import React, { useState } from "react"
import TextField from "@material-ui/core/TextField"
import { useFormik } from "formik"
import { Alert } from "reactstrap"
import { sendEmail } from "../../utils/email"
import * as yup from "yup"

export const validationSchema = yup.object().shape({
  email: yup.string().email("Niepoprawny email").required("Pole jest wymagane"),
  password: yup
    .string()
    .min(6, "The password is too short")
    .max(12, "The password is too long")
    .required("This field is required"),
})

export const ShowFormik = () => {
  const [isComplete, setIsCompleted] = useState(false)
  const { values, handleChange, handleSubmit, errors } = useFormik({
    initialValues: {
      name: "",
      lastName: "",
      class: "",
      parentName: "",
      parentLastName: "",
      email: "",
      phone: "",
      localization: "",
    },
    onSubmit: values => {
      sendEmail(values).then(r => setIsCompleted(true))
    },
  })

  const sendEmail = async values => {
    console.log(values)
    var myHeaders = new Headers()
    myHeaders.append("Content-Type", "application/json")

    var raw = JSON.stringify({
      values,
    })

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    }

    fetch("https://fifamailer.herokuapp.com/send", requestOptions)
      .then(response => response.text())
      .then(result => {
        setIsCompleted(true)
        console.log(result)
      })
      .catch(error => console.log("error", error))
  }

  const defaultProps = {
    onChange: handleChange,
    variant: "filled",
    required: true,
  }

  return (
    <>
      {isComplete ? (
        <Alert color="primary">
          Mail został wysłany skontaktujemy się wkrótce
        </Alert>
      ) : (
        <form onSubmit={handleSubmit} className="showFormik--container">
          <div className="d-flex justify-content-between underLabel">
            <TextField
              label="Imię"
              name="name"
              value={values.name}
              {...defaultProps}
            />
            <TextField
              name="lastName"
              value={values.lastName}
              {...defaultProps}
              label="Nazwisko"
            />
          </div>
          <TextField label="Klasa" variant="filled" {...defaultProps} />
          <div className="d-flex justify-content-between underLabel">
            <TextField
              name="parentName"
              label="Imię rodzica"
              variant="filled"
              value={values.parentName}
              {...defaultProps}
            />
            <TextField
              value={values.parentLastName}
              name="parentLastName"
              label="Nazwisko rodzica"
              variant="filled"
              {...defaultProps}
            />
          </div>
          <div className="d-flex justify-content-between underLabel">
            <TextField
              value={values.email}
              type="email"
              name="email"
              {...defaultProps}
              label="Email rodzica"
              variant="filled"
            />
            <TextField
              value={values.phone}
              name="phone"
              {...defaultProps}
              label="Numer telefonu"
              variant="filled"
            />
          </div>
          <TextField
            {...defaultProps}
            value={values.localization}
            name="localization"
            label="Preferowana lokalizacja"
            variant="filled"
          />
          <div className="showFormik--container--button">
            <button type="submit" className="twoButtonContainer__button red">
              Zapisz się
            </button>
          </div>
        </form>
      )}
    </>
  )
}
