import React from "react"

export const ModalHero = ({ src, title }) => {
  return (
    <div className="modalHero--container">
      <img className="modalHero--container__img" src={src} />
      <h1 className="modalHero--container__title">
        Zapisz się na zajęcia pokazowe!
      </h1>
      <p className="text-center">
        Podaj dane. Nawiążemy kontakt i ustalimy szczegóły.
      </p>
    </div>
  )
}
