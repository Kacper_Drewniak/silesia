import React, { useState } from "react"
import TextField from "@material-ui/core/TextField"
import { useFormik } from "formik"
import { Alert } from "reactstrap"

export const ContactFormik = ({ title }) => {
  const [isComplete, setIsCompleted] = useState(false)
  const { values, handleChange, handleSubmit } = useFormik({
    initialValues: {
      name: "",
      lastName: "",
      email: "",
      phone: "",
      message: "",
    },
    onSubmit: () => sendEmail(values),
  })

  const sendEmail = async values => {
    console.log(values)
    var myHeaders = new Headers()
    myHeaders.append("Content-Type", "application/json")

    var raw = JSON.stringify({
      values,
    })

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    }

    fetch("https://fifamailer.herokuapp.com/send", requestOptions)
      .then(response => response.text())
      .then(result => {
        setIsCompleted(true)
        console.log(result)
      })
      .catch(error => console.log("error", error))
  }

  const defaultProps = {
    onChange: handleChange,
    variant: "filled",
  }

  return (
    <>
      {isComplete ? (
        <Alert color="primary">
          Mail został wysłany skontaktujemy się wkrótce
        </Alert>
      ) : (
        <form onSubmit={handleSubmit} className="showFormik--container">
          <div className="d-flex justify-content-between underLabel">
            <TextField
              label="Imię"
              name="name"
              value={values.name}
              {...defaultProps}
            />
            <TextField
              name="lastName"
              value={values.lastName}
              {...defaultProps}
              label="Nazwisko"
            />
          </div>
          <div className="d-flex justify-content-between underLabel">
            <TextField
              {...defaultProps}
              name="email"
              value={values.email}
              label="Email"
              variant="filled"
            />
            <TextField
              {...defaultProps}
              name="phone"
              value={values.phone}
              label="Numer telefonu"
              variant="filled"
            />
          </div>
          <TextField
            {...defaultProps}
            name="message"
            value={values.message}
            label="Wiadomość"
            multiline={true}
            variant="filled"
          />
          <div className="showFormik--container--button">
            <button type="submit" className="twoButtonContainer__button red">
              {title ?? "Skontaktuj się!"}
            </button>
          </div>
        </form>
      )}
    </>
  )
}
