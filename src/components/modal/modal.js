import React from "react"
import { ModalHero } from "./modalHero"
import { ShowFormik } from "./showFormik"

export const Modal = ({ isActive, closeModal, hero, formik }) => {
  return (
    <div  class={`modalShadow  ${isActive && "active"}`}>
      <div className="modalShadow--container">
        <img
          onClick={closeModal}
          className="modalShadow--container__x"
          src="/images/xmark.svg"
        />
        {hero}
        {formik}
      </div>
    </div>
  )
}
