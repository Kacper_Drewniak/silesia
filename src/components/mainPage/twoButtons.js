import React from "react"
import { Link } from "gatsby"

export const TwoButtons = () => {
  return (
    <div className="twoButtonContainer">
      <a
        className="twoButtonContainer__button red"
        href="https://sklep.silesia3d.pl/pl"
      >
        Sklep
      </a>
      <Link className="twoButtonContainer__button blue" to="oferta">
        Oferta
      </Link>
    </div>
  )
}
