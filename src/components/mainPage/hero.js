import React, { useState } from "react"
import { Modal } from "../modal/modal"
import { ModalHero } from "../modal/modalHero"
import { ShowFormik } from "../modal/showFormik"
import { CycleFormik } from "../modal/cycleFormik"
import { Link } from "gatsby"

export const Hero = () => {
  const [isActive, setIsActive] = useState(false)
  const [isCycleActive, setIsCycleAcitve] = useState(false)
  return (
    <>
      <Modal
        isActive={isActive}
        hero={
          <ModalHero
            src="/images/modals/pokazowe.png"
            title=" Zapisz się na zajęcia pokazowe!"
          />
        }
        formik={<ShowFormik />}
        closeModal={() => setIsActive(false)}
      />
      <div className="heroContainer">
        <h1 className="heroContainer__title">
          Zajęcia z druku 3D dla dzieci w wieku 7-14 lat, to dobry start w
          przyszłość!
        </h1>
        <div className="heroContainer__background">
          <a
            className="heroContainer__buttonContainer bgGreen"
            onClick={() => setIsActive(true)}
          >
            <span className="heroContainer__buttonContainer__content">
              Zapisz się na
              <br /> zajęcia próbne
            </span>
            <img
              className="heroContainer__buttonContainer__icon"
              src="/images/chevron-right.svg"
            />
          </a>
          <Link
            className="heroContainer__buttonContainer bgYellow"
            to="/zajecia-cykliczne"
          >
            <span className="heroContainer__buttonContainer__content">
              Zapisz się na
              <br /> zajęcia cykliczne
            </span>
            <img
              className="heroContainer__buttonContainer__icon"
              src="/images/chevron-right.svg"
            />
          </Link>
        </div>
        <img
          src="/images/child.png"
          className="heroContainer__background__avatar"
        />
      </div>
    </>
  )
}
