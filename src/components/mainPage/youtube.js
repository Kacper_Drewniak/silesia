import React from "react"

export const Youtube = () => {
  return (
    <div className="text-center">
      <iframe
        id="youtubeContainer__iframe"
        height="412"
        src="https://www.youtube.com/embed/zDub_T2F7_w"
        title="YouTube video player"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      ></iframe>
    </div>
  )
}
