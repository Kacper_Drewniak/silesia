import React from "react"
import { Container } from "reactstrap"
import { TwoButtons } from "./twoButtons"
import { Youtube } from "./youtube"
import { aboutLessons } from "../../data"
import { Hero } from "./hero"
export const Welcome = () => {
  return (
    <Container fluid className="welcomeContainer">
      <h1 className="welcomeContainer--title">Witaj w Edu3Dkacja!</h1>
      <TwoButtons />
      <Youtube />
      <h2 className="welcomeContainer--subTitle">Zajęcia z druku 3D</h2>
      <p className="welcomeContainer--description">{aboutLessons}</p>
      <Hero />
    </Container>
  )
}
