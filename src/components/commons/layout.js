import React from "react"
import { Nav } from "./nav"
import { Footer } from "./footer"

export const Layout = ({ children }) => {
  return (
    <>
      <Nav />
      {children}
      <Footer />
    </>
  )
}
