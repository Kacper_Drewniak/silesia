import React from "react"
import { footerLinks } from "../../data"
import { Link } from "gatsby"

export const Footer = () => {
  return (
    <footer className="footerContainer">
      {footerLinks.map(({ icon, label }) => (
        <Link className="footerContainer--link" to="#">
          {icon && (
            <img
              src={`/images/${icon}.svg`}
              className="footerContainer--icon"
            />
          )}
          {label}
        </Link>
      ))}
    </footer>
  )
}
