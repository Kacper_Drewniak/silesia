import React from "react"

export const ImageText = ({ text, theme }) => {
  return <header className={`headerContainer ${theme}`}>{text}</header>
}
