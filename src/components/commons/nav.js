import React, { useState } from "react"
import { navLinks } from "../../data"
import { Link } from "gatsby"
import { Modal } from "../modal/modal"
import { ModalHero } from "../modal/modalHero"
import { CycleFormik } from "../modal/cycleFormik"
import { ShowFormik } from "../modal/showFormik"
import Hamburger from "hamburger-react"
export const Nav = () => {
  const [isOpen, setIsOpen] = useState(false)
  const [isPokazoweActive, setIsPokazoweActive] = useState(false)
  const [isCykliczneActive, setIsCykliczneActive] = useState(false)
  return (
    <>
      <nav className="mobileNacContainer">
        <a href="/">
          <img src="/images/logo.svg" />
        </a>
        <Hamburger toggled={isOpen} toggle={setIsOpen} />
        <div className={`mobileNavbar ${isOpen && "active"}`}>
          {navLinks.map(({ label, url, outsideLink, modal }) => (
            <>
              {outsideLink ? (
                <a className="navContainer__menuContainer--link" href={url}>
                  {label}
                </a>
              ) : (
                <>
                  {label === "Zajęcia pokazowe" && (
                    <a
                      className="navContainer__menuContainer--link"
                      onClick={() => setIsPokazoweActive(true)}
                    >
                      {label}
                    </a>
                  )}

                  {label !== "Zajęcia pokazowe" && (
                    <Link
                      className="navContainer__menuContainer--link"
                      to={url}
                      activeClassName="active"
                    >
                      {label}
                    </Link>
                  )}
                </>
              )}
            </>
          ))}
        </div>
      </nav>
      <nav className="navContainer">
        <Modal
          isActive={isPokazoweActive}
          hero={
            <ModalHero
              src="/images/modals/pokazowe.png"
              title=" Zapisz się na zajęcia pokazowe!"
            />
          }
          formik={<ShowFormik />}
          closeModal={() => setIsPokazoweActive(false)}
        />
        <div className="navContainer__logoContainer">
          <a href="/" className="navContainer__logoContainer--img">
            <img src="/images/logo.svg" />
          </a>
        </div>
        <div className="navContainer__menuContainer">
          {navLinks.map(({ label, url, outsideLink, modal }) => (
            <>
              {outsideLink ? (
                <a className="navContainer__menuContainer--link" href={url}>
                  {label}
                </a>
              ) : (
                <>
                  {label === "Zajęcia pokazowe" && (
                    <a
                      className="navContainer__menuContainer--link"
                      onClick={() => setIsPokazoweActive(true)}
                    >
                      {label}
                    </a>
                  )}

                  {label !== "Zajęcia pokazowe" && (
                    <Link
                      className="navContainer__menuContainer--link"
                      to={url}
                      activeClassName="active"
                    >
                      {label}
                    </Link>
                  )}
                </>
              )}
            </>
          ))}
        </div>
      </nav>
    </>
  )
}
