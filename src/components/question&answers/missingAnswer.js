import React, { useState } from "react"
import { Modal } from "../modal/modal"
import { ContactFormik } from "../modal/contactFormik"

export const MissingAnswer = () => {
  const [isActive, setIsActive] = useState(false)
  return (
    <>
      <Modal
        isActive={isActive}
        formik={<ContactFormik />}
        hero={
          <h1 className="m-4 text-center">
            Śmiało zadaj pytanie! <br />
            Odpowiemy w niedługim czasie ;)
          </h1>
        }
        closeModal={() => setIsActive(false)}
      />
      <div className="missingAnswer--container">
        <h1 className="missingAnswer--container__title">BRAK ODPOWIEDZI?</h1>
        <p className="missingAnswer--container__description">
          Nie wahaj się! Zadaj nam pytanie a my na nie odpowiemy. Pamiętaj lecz
          by sprawdzić czy odpowiedź nie znajduje się już u nas.
        </p>
        <a
          onClick={() => setIsActive(true)}
          className="twoButtonContainer__button red"
        >
          ZADAJ PYTANIE
        </a>
      </div>
    </>
  )
}
