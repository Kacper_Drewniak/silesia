import React from "react"
import Collapsible from "react-collapsible"
import { Container } from "reactstrap"

export const QuestionAnswer = ({ question, answer }) => {
  return (
    <Collapsible
      trigger={
        <div
          style={{
            fontSize: "24px",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <div>{question}</div>
          <img className="collapseIcon" src="/images/caret.svg" width="16px" />
        </div>
      }
    >
      <div className="p-4">{answer}</div>
    </Collapsible>
  )
}

export const QuestionList = () => {
  const list = [
    {
      question: "Jakie rodzaje zajęć organizujecie?",
      answer: (
        <>
          Prowadzimy zajęcia związane z projektowaniem, drukowaniem i
          skanowaniem 3D, zajmujemy się też produktem VR. Powyższe zajęcia
          organizowane są w formie cyklicznej jak i jednorazowej. Tematyka
          jednorazowych warsztatów to na przykład: Wielkanoc, Boże Narodzenie,
          Dzień Kobiet/Mężczyzn, Halloween, Dzień dziecka, Dzień babci/dziadka i
          wiele innych! Zapraszamy do naszej zakładki oferta, aby poznać więcej
          szczegółów.
        </>
      ),
    },
    {
      question:
        "Gdzie organizujecie zajęcia cykliczne z drukowania i projektowania 3D?",
      answer: (
        <>
          Zajęcia cykliczne mogą być zorganizowane za równo w szkołach
          podstawowych, jak i w miejskich placówkach - domach kultury. Jeśli
          twojej szkoły/MDK-u nie ma na naszej liście zajęć cyklicznych
          skontaktuj się z nami a zorganizujemy tam zajęcia pokazowe!
        </>
      ),
    },
    {
      question: "Jak zapisać się na zajęcia cykliczne? ",
      answer: (
        <>
          Zapisu można dokonać przez naszą stronę internetową - zakładka zajęcia
          cykliczne. Po wypełnieniu formularza Państwa zgłoszenie zostanie
          wysłane do naszego systemu Active now - tam zatwierdzimy zapis i
          dodamy Państwa dziecko do grupy. Jeśli twojej szkoły/MDK-u nie ma na
          naszej liście zajęć cyklicznych skontaktuj się z nami a zorganizujemy
          tam zajęcia pokazowe!
        </>
      ),
    },
    {
      question: "Jak długo trwają zajęcia cykliczne?",
      answer: (
        <>
          Nasze zajęcia cykliczne, jak sama nazwa wskazuje, opierają się na
          cyklach - jeden cykl to cztery spotkania. Jedno spotkanie z zajęć
          cyklicznych trwa 75 minut. Po zakończonym cyklu można kontynuować
          swoje uczestnictwo i brać udział w kolejnym cyklu/cyklach. Mamy
          przygotowane scenariusze zajęć aż na dwa lata - 4 pełne semestry!
        </>
      ),
    },
    {
      question: "Dla dzieci w jakim wieku przewidziane są zajęcia z druku 3D?",
      answer: (
        <>
          Na nasze zajęcia przeważnie zapraszamy dzieci w wieku 7-14 lat.
          Organizujemy również zajęcia dla dzieci w wieku przedszkolnym{" "}
        </>
      ),
    },
    {
      question: "Jak duże są grupy dzieci na zajęciach cyklicznych?",
      answer: <>Grupy liczą zwykle do 15 osób. </>,
    },
    {
      question:
        "Startuje nowa grupa zajęć cyklicznych. Kiedy muszę dokonać płatności za pierwszy cykl?",
      answer: (
        <>
          Płatność można zrealizować w dniu pierwszych zajęć cyklicznych lub po
          pierwszym spotkaniu, gdy dziecko wróci zadowolone z naszych zajęć 🙂{" "}
        </>
      ),
    },
    {
      question:
        "Gdzie mogę zobaczyć terminy przyszłych zajęć cyklicznych i informacje dotyczące płatności za udział mojego dziecka?",
      answer: (
        <>
          Wszelkie informacje można śledzić w panelu klienta Active now. Po
          zatwierdzeniu zapisu na zajęcia otrzymają Państwo maila z zaproszeniem
          do rejestracji w systemie Active now.
        </>
      ),
    },
    {
      question: "Jakie formy płatności obowiązują?",
      answer: (
        <>
          Za udział dziecka w naszych zajęciach można zapłacić online poprzez
          tpay w panelu klienta Active now, lub przelewem tradycyjnym (dane do
          przelewu w zakładce kontakt).
        </>
      ),
    },
    {
      question:
        "Jakie informacje muszę wpisać w tytule przelewu tradycyjnego? ",
      answer: (
        <>
          Bardzo prosimy o podanie imienia i nazwiska dziecka, szkoły/MDK-u oraz
          grupy, do której uczęszcza lub nazwy wydarzenia jednorazowego, w
          którym weźmie udział.{" "}
        </>
      ),
    },
    {
      question:
        "Wiem, że moje dziecko nie będzie mogło pojawić się na kolejnych zajęciach, czy mam kogoś o tym poinformować? ",
      answer: (
        <>
          Tak, bardzo prosimy o informację drogą mailową, że Państwa dziecko nie
          będzie mogło wziąć udziału w zbliżających się zajęciach. Nieobecność
          można też zgłosić w panelu klienta Active now. Dzięki temu będziemy
          wiedzieć z wyprzedzeniem ilu dzieci nie będzie, a jeśli będzie to
          większość grupy - odwołamy dane spotkanie i przesuniemy je na kolejny
          tydzień tak aby uczestnicy nie stracili zajęć.{" "}
        </>
      ),
    },
    {
      question:
        "Dziecko nie mogło pojawić się na jednych z zajęć cyklicznych. Czy otrzymam zwrot pieniędzy za te zajęcia lub płatność zostanie przesunięta na kolejne?",
      answer: (
        <>
          Niestety, zgodnie z naszym regulaminem nie ma możliwości zwrotu
          kosztów zajęć w momencie gdy dziecko opuści któreś z nich. Pojedyncza
          płatność zostaje przesunięta na kolejne zajęcia jedynie w momencie gdy
          zajęcia nie będą mogły się odbyć z naszej winy - organizatorów zajęć.{" "}
        </>
      ),
    },
    {
      question: "Czy można odrabiać zajęcia jeśli dziecko było nieobecne? \n",
      answer: (
        <>
          Dany projekt można odrobić na zajęciach własnej grupy - po
          zaprojektowaniu aktualnego projektu z zajęć z całą grupą, dziecko może
          zacząć odrabiać swój zaległy projekt. Drugą opcją jest odrobienie
          zajęć w innej szkole z grupą, która akurat będzie dany projekt
          realizowała. Jeśli chcieliby Państwo aby dziecko odrobiło zaległy
          projekt, prosimy o taką informację drogą mailową, a postaramy się
          znaleźć dogodne rozwiązanie.
        </>
      ),
    },
    {
      question:
        "Co jeśli znowu szkoły przejdą na nauczanie w formie zdalnej, w związku z pandemią Covid-19? \n",
      answer: (
        <>
          Jesteśmy przygotowani na taką ewentualność i jeśli wystąpi
          konieczność, nasze zajęcia z druku 3D będą odbywały się zdalnie, aby
          uczestnicy nie tracili zajęć.{" "}
        </>
      ),
    },
  ]

  return (
    <Container className="QL--container">
      {list.map(({ question, answer }) => {
        return <QuestionAnswer question={question} answer={answer} />
      })}
    </Container>
  )
}
