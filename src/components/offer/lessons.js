import { Container } from "reactstrap"
import React, { useState } from "react"
import { Modal } from "../modal/modal"
import { ModalHero } from "../modal/modalHero"
import { ShowFormik } from "../modal/showFormik"
import { CycleFormik } from "../modal/cycleFormik"
import { ContactFormik } from "../modal/contactFormik"
import { Link } from "gatsby"
export const Lessons = () => {
  const [isPokazoweActive, setIsPokazoweActive] = useState(false)
  const [isSeniorActive, setIsSeniorActive] = useState(false)
  const [isVrActive, setIsVrActive] = useState(false)
  const lessons = [
    {
      button: (
        <button
          onClick={() => setIsPokazoweActive(true)}
          className="twoButtonContainer__button blue"
        >
          Zapisz się
        </button>
      ),
      title: "Zajęcia pokazowe",
      text: "Zajęcia mają na celu zainteresowanie dzieci drukiem i projektowaniem 3D, wirtualnymi okularami  oraz innymi nowoczesnymi technologiami. Podczas spotkań w sposób kompleksowy przedstawimy całość procesu powstania przedmiotu – od naszego pomysłu po finalny wydruk w technologii 3D. Podczas spotkań z historią i przyrodą zapewniamy dzieciom podróż w nieznane tajemnice Ziemi. Zajęcia gwarantują praktyczną naukę poprzez zabawę.",
    },
    {
      button: (
        <Link
          to="/zajecia-cykliczne"
          className="twoButtonContainer__button red"
        >
          Zapisz się
        </Link>
      ),
      title: "Zajęcia Cykliczne",
      text: "Cykliczne warsztaty z druku 3D dla dzieci już od 7 roku życia. Zajęcia obejmują trzy bloki tematyczne: projektowanie 3D, skanowanie 3D i drukowanie 3D. Na naszych zajęciach młodzi konstruktorzy uczą się projektować przedmioty, które następnie są dla nich wytwarzane na drukarkach 3D.Nasze urządzenia są mobilne, możemy zorganizować warsztaty w wybranej lokalizacji np. Twojej szkole.",
    },

    {
      button: (
        <button
          onClick={() => setIsVrActive(true)}
          className="twoButtonContainer__button red"
        >
          Zapisz się
        </button>
      ),
      title: "Wirtualna Rzeczywistość",
      text: "Nowatorskie spotkania z wykorzystaniem okularów pozwalają w pełni zaangażwoać dziecko w naukę, która staje się świetną zabawą. Wyścig do serca, podróż do gwiazd, czy zwiedzanie miast to nie tylko trójwymiarowe obiekty i złożone struktury na wyciągnięcie ręki, to przede wszystkim prawdziwa i realna wiedza, która pokazuje nasze życie we wszystkich obszarach aktywności ludzkiej. Przyjdź i wraz z nami zanórz się w magiczny świat rzeczywistości wirtualnej.",
    },
    {
      button: (
        <button
          onClick={() => setIsSeniorActive(true)}
          className="twoButtonContainer__button blue"
        >
          Zapisz się
        </button>
      ),
      title: "Zajęcia dla seniorów",
      text: "Drukować każdy może, czyli cykliczne warsztaty dla Seniorów z druku 3D. uczestnicy uczą się projektowania przedmiotów, które następnie są dla nich wytwarzane w drukarkach 3D. Nasze urządzenia są mobile przez co możemy zorganizować warsztaty w wybranej lokalizacji.",
    },
  ]

  const Item = ({ title, text, imgSrc, button, revert }) => (
    <div className={`lessonsContainer__itemContainer`}>
      <img src={imgSrc} className="lessonsContainer__itemContainer--image" />
      <div className={`lessonsContainer__itemContainer--content`}>
        <h1 className="lessonsContainer__itemContainer--title">{title}</h1>
        <p className="lessonsContainer__itemContainer--description">{text}</p>
        {button}
      </div>
    </div>
  )

  return (
    <>
      <Modal
        isActive={isVrActive}
        hero={
          <>
            <h1 className="m-4 text-center">Zapraszamy na zajęcia VR!</h1>
            <p className="text-center">
              Podaj dane. Nawiążemy kontakt i ustalimy szczegóły
            </p>
          </>
        }
        formik={<ContactFormik />}
        closeModal={() => setIsVrActive(false)}
      />
      <Modal
        isActive={isSeniorActive}
        hero={
          <h1 className="m-4 text-center">
            Zapraszamy dla zajęcia dla seniorów!
          </h1>
        }
        formik={<ContactFormik title="Zapisz się" />}
        closeModal={() => setIsSeniorActive(false)}
      />
      <Modal
        isActive={isPokazoweActive}
        hero={
          <ModalHero
            src="/images/modals/pokazowe.png"
            title=" Zapisz się na zajęcia pokazowe!"
          />
        }
        formik={<ShowFormik />}
        closeModal={() => setIsPokazoweActive(false)}
      />
      <Container className="lessonsContainer">
        {lessons.map(({ imgSrc, title, text, button }, index) => (
          <Item
            imgSrc={`/images/lessons/item${index}.png`}
            title={title}
            text={text}
            revert={index % 2}
            button={button}
          />
        ))}
      </Container>
    </>
  )
}
