import React, { useState } from "react"
import CrossfadeCarousel from "@notbaldrick/react-crossfade-carousel"
import { Container } from "reactstrap"
export const Advantages = () => {
  const images = [
    "/images/advantages/brain.png",
    "/images/advantages/light.png",
    "/images/advantages/pen.png",
    "/images/advantages/screen.png",
    "/images/advantages/tea.png",
    "/images/advantages/tech.png",
    "/images/advantages/work.png",
  ]

  return (
    <Container className="advantagesContainer">
      <h1 className="advantagesContainer--title"></h1>
      <div className="advantagesContainer__CrossContainer">
        <CrossfadeCarousel
          interval={3000}
          transition={2000}
          images={images}
          imageStyles={{
            backgroundSize: "contain",
            backgroundRepeat: "no-repeat",
          }}
        />
      </div>
    </Container>
  )
}
