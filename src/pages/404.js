import * as React from "react"

import Layout from "../components/commons/layout"
import Seo from "../components/seo"

const NotFoundPage = () => <> not found</>

export default NotFoundPage
