import React, { useState } from "react"
import { Layout } from "../components/commons/layout"
import { Hero } from "../components/mainPage/hero"
import { HeroLaboratory } from "../components/laboratory/HeroLaboratory"
import { ElementLaboratory } from "../components/laboratory/elementLaboratory"
import { TogetherLaboratory } from "../components/laboratory/togetherLaboratory"
import { ImageText } from "../components/commons/imageText"
import { Modal } from "../components/modal/modal"
import { ContactFormik } from "../components/modal/contactFormik"

const LaboratoriaPrzyszlosci = () => {
  const [isActive, setIsActive] = useState(false)

  return (
    <Layout>
      <Modal
        isActive={isActive}
        formik={<ContactFormik />}
        hero={
          <h1 className="m-4 text-center">
            Śmiało zadaj pytanie! <br />
            Odpowiemy w niedługim czasie ;)
          </h1>
        }
        closeModal={() => setIsActive(false)}
      />
      <ImageText theme="laboratory" text="laboratoria przyszlosci" />
      <HeroLaboratory />
      <ElementLaboratory />
      <TogetherLaboratory />
      <div className="offer-button-container m-4">
        <a
          onClick={() => setIsActive(true)}
          className="twoButtonContainer__button blue"
        >
          prześlij formularz zapisowy!
        </a>
      </div>
    </Layout>
  )
}

export default LaboratoriaPrzyszlosci
