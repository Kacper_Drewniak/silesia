import React from "react"
import { Layout } from "../components/commons/layout"
import { Welcome } from "../components/mainPage/welcome"

const IndexPage = () => (
  <Layout>
    <Welcome />
  </Layout>
)

export default IndexPage
