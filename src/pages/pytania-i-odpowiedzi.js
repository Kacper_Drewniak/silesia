import React from "react"
import { Layout } from "../components/commons/layout"
import { MissingAnswer } from "../components/question&answers/missingAnswer"
import { Container } from "reactstrap"
import { QuestionList } from "../components/question&answers/questionList"

const index = () => {
  return (
    <Layout>
      <Container>
        <MissingAnswer />
        <QuestionList />
      </Container>
    </Layout>
  )
}

export default index
