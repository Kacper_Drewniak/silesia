import React from "react"
import { Layout } from "../components/commons/layout"
import { Content } from "../components/terms/content"

const Regulamin = () => {
  return (
    <Layout>
      <Content />
    </Layout>
  )
}

export default Regulamin
