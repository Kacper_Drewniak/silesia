import React from "react"
import { Layout } from "../components/commons/layout"
import { ImageText } from "../components/commons/imageText"
import { Advantages } from "../components/offer/advantages"
import { Lessons } from "../components/offer/lessons"
import { Link } from "gatsby"

const Oferta = () => (
  <Layout>
    <ImageText text="nauka przez zabawę" />

    <Lessons />
    <Advantages />
    <div className="offer-button-container">
      <Link to="/konspekty-zajec" className="btn-show-conspect">
        ZOBACZ TAKŻE KONSPEKTY ZAJĘĆ
      </Link>
    </div>
  </Layout>
)

export default Oferta
