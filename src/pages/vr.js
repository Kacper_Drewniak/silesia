import React, { useState } from "react"
import { Layout } from "../components/commons/layout"
import { Link } from "gatsby"
import { Modal } from "../components/modal/modal"
import { ContactFormik } from "../components/modal/contactFormik"
import { ModalHero } from "../components/modal/modalHero"
import { ShowFormik } from "../components/modal/showFormik"
import { Container } from "reactstrap"

const Vr = () => {
  const [isPokazoweActive, setIsPokazoweActive] = useState(false)
  const [isSeniorActive, setIsSeniorActive] = useState(false)
  const [isVrActive, setIsVrActive] = useState(false)
  const lessons = [
    {
      button: (
        <button
          onClick={() => setIsVrActive(true)}
          className="twoButtonContainer__button red"
        >
          Zapisz się
        </button>
      ),
      title: "Wirtualna Rzeczywistość",
      text: "Nowatorskie spotkania z wykorzystaniem okularów pozwalają w pełni zaangażwoać dziecko w naukę, która staje się świetną zabawą. Wyścig do serca, podróż do gwiazd, czy zwiedzanie miast to nie tylko trójwymiarowe obiekty i złożone struktury na wyciągnięcie ręki, to przede wszystkim prawdziwa i realna wiedza, która pokazuje nasze życie we wszystkich obszarach aktywności ludzkiej. Przyjdź i wraz z nami zanórz się w magiczny świat rzeczywistości wirtualnej.",
    },
  ]

  const Item = ({ title, text, imgSrc, button, revert }) => (
    <div className={`lessonsContainer__itemContainer`}>
      <img src={imgSrc} className="lessonsContainer__itemContainer--image" />
      <div className={`lessonsContainer__itemContainer--content`}>
        <h1 className="lessonsContainer__itemContainer--title">{title}</h1>
        <p className="lessonsContainer__itemContainer--description">{text}</p>
        {button}
      </div>
    </div>
  )

  return (
    <Layout>
      <Modal
        isActive={isVrActive}
        hero={
          <>
            <h1 className="m-4 text-center">Zapraszamy na zajęcia VR!</h1>
            <p className="text-center">
              Podaj dane. Nawiążemy kontakt i ustalimy szczegóły
            </p>
          </>
        }
        formik={<ContactFormik />}
        closeModal={() => setIsVrActive(false)}
      />
      <Modal
        isActive={isSeniorActive}
        hero={
          <h1 className="m-4 text-center">
            Zapraszamy dla zajęcia dla seniorów!
          </h1>
        }
        formik={<ContactFormik title="Zapisz się" />}
        closeModal={() => setIsSeniorActive(false)}
      />
      <Modal
        isActive={isPokazoweActive}
        hero={
          <ModalHero
            src="/images/modals/pokazowe.png"
            title=" Zapisz się na zajęcia pokazowe!"
          />
        }
        formik={<ShowFormik />}
        closeModal={() => setIsPokazoweActive(false)}
      />
      <Container className="lessonsContainer " style={{ margin: "12rem 0" }}>
        {lessons.map(({ imgSrc, title, text, button }, index) => (
          <Item
            imgSrc={`/images/lessons/item2.png`}
            title={title}
            text={text}
            revert={index % 2}
            button={button}
          />
        ))}
      </Container>
    </Layout>
  )
}

export default Vr
