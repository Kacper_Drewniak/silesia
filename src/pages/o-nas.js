import React from "react"
import { Layout } from "../components/commons/layout"
import { ImageText } from "../components/commons/imageText"
import { OurTeam } from "../components/aboutUs/ourTeam"

const ONas = () => (
  <Layout>
    <ImageText theme="onas" text="nauka przez zabawę" />
    <OurTeam />
  </Layout>
)

export default ONas
