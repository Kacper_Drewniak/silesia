import React from "react"
import { Layout } from "../components/commons/layout"
import { Container } from "reactstrap"
import { CycleFormik } from "../components/modal/cycleFormik"

const index = () => {
  return (
    <Layout>
      <Container>
        <div className="text-center">
          <button
            className="twoButtonContainer__button red small"
            onClick={() => window.location.reload()}
          >
            Jeśli formularz się nie ładuje prosimy kliknąć ten przecisk
          </button>
        </div>
        <CycleFormik />
      </Container>
    </Layout>
  )
}

export default index
