import React from "react"
import { Layout } from "../components/commons/layout"
import { Container } from "reactstrap"
import { FourButtons } from "../components/contact/fourButtons"
import { HeroContact } from "../components/contact/heroContact"
import axios from "axios"

const Kontakt = () => {
  return (
    <Layout>
      <Container className="contact--container">
        <FourButtons />
        <HeroContact />

        <div className="contactHeroText">
          <h1 className="modalHero--container__title">Dane firmy:</h1>
          <p>
            Numer rachunku bankowego do wpłat: 70 1050 1399 1000 0090 8137 3079
            <br />
            Nazwa firmy: WBJ SZKOLENIA SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ
            <br />
            Adres firmy UL.SAPERSKA 2A, 43-200 PSZCZYNA
          </p>
          <br />
          <p>
            WBJ Szkolenia Sp. z o. o.
            <br />
            Ul. Saperska 2a
            <br />
            43-200 Pszczyna
            <br />
            NIP: 6381830114
            <br />
            Regon:368911720
            <br />
            KRS: 0000707761
          </p>
        </div>
      </Container>
    </Layout>
  )
}

export default Kontakt
