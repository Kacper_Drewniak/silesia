export const navLinks = [
  {
    label: "Strona główna",
    url: "/",
    modal: false,
    outsideLink: false,
  },
  {
    label: "Oferta",
    url: "/oferta",
    modal: false,
    outsideLink: false,
  },
  {
    label: "O nas",
    url: "/o-nas",
    modal: false,
    outsideLink: false,
  },
  {
    label: "Zajęcia pokazowe",
    url: "/zajecia-pokazowe",
    modal: false,
    outsideLink: false,
  },
  {
    label: "Zajęcia cykliczne",
    url: "/zajecia-cykliczne",
    modal: true,
    outsideLink: false,
  },
  {
    label: "VR",
    url: "/vr",
    modal: false,
    outsideLink: false,
  },
  {
    label: "Konspekty zajęć",
    url: "/konspekty-zajec",
    modal: false,
    outsideLink: false,
  },
  {
    label: "Pytania i odpowiedzi",
    url: "/pytania-i-odpowiedzi",
    modal: false,
    outsideLink: false,
  },
  {
    label: "Laboratoria przyszłości",
    url: "/laboratoria-przyszlosci",
    modal: false,
    outsideLink: false,
  },
  {
    label: "Kontakt",
    url: "/kontakt",
    modal: false,
    outsideLink: false,
  },
  {
    label: "Regulamin",
    url: "/regulamin",
    modal: false,
    outsideLink: false,
  },
  {
    label: "Sklep",
    modal: false,
    url: "https://sklep.silesia3d.pl/pl",
    outsideLink: true,
  },
]

export const footerLinks = [
  {
    icon: "planet",
    label: "www.silesia3d.pl",
  },
  {
    icon: "mail",
    label: "biuro@silesia3d.pl",
  },
  {
    icon: "phone",
    label: "533 572 925",
  },
  {
    icon: "fb",
    label: "Edu3Dkacja Silesia",
  },
  {
    icon: "insta",
    label: "@edu3dkacja.silesia",
  },
  {
    label: "© EDU3DKACJA SILESIA 2022r.",
  },
]

export const aboutLessons =
  "Wspierające rozwój dzieci w wieku 7-14 lat W Edu3Dkacja prowadzimy zajęcia dla dzieci,\n" +
  "które rozwijają wyobraźnie przestrzenną i pozwalają na poznanie najnowocześniejszych technologii.\n" +
  "Czy wiesz, że technologia druku 3D jest uważana za jedną z najbardziej przyszłościowych dziedzin rozwoju przemysłu na świecie?\n" +
  "Już dziś druk 3D jest wykorzystywany w wielu branżach: w medycynie, w archeologii, motoryzacji, lotnictwie, budownictwie\n" +
  "a to dopiero POCZĄTEK ery druku 3D!"
